/**
 * BT2: Tính giá trị trung bình.
 * 
 * Input:
 * - Người dùng nhập vào 5 số thực:
 * - SoThuc1 = 2;
 * - SoThuc2 = 8;
 * - SoThuc3 = 4;
 * - SoThuc4 = 5;
 * - SoThuc5 = 9;
 * 
 * Todo:
 * S1: Tạo 5 biến lưu Số thực.
 * S2: Tạo một biến lưu kết quả.
 * S3: Tính Trung Bình Cộng 5 số thực:
 * - ( SoThuc1 + SoThuc2 + SoThuc3 + SoThuc4 + SoThuc5) / 5;
 * 
 * 
 * OutPut:
 * - Kết quả trả về màn hình là: 5.6
 * 
 *   */ 
var SoThuc1 = 2;
var SoThuc2 = 8;
var SoThuc3 = 4;
var SoThuc4 = 5;
var SoThuc5 = 9;

var KQ = null;

console.log("BT2: Tính giá trị trung bình.");
KQ = (SoThuc1+SoThuc2+SoThuc3+SoThuc4+SoThuc5) / 5;
console.log('Giá trị trung bình là: ', KQ);

