/**
 * BT5: Tính tổng 2 ký số
 * 
 * 
 * Input:
 * - Người dùng nhập vào 1 số có 2 chữ số (vd: 12, 24, 36)
 * - SoNhapVao = 18;
 * 
 * Todo:
 * S1: Tạo 1 biến lưu kết quả người dùng nhập vào.
 * S2: Tạo 2 biến lấy số hàng chục và hàng đơn vị
 * - HangDV = SoNhapVao % 10;
 * - HangChuc = Math.floor(SoNhapVao/10);
 * S3: Tạo 1 biến lưu kết quả
 * - KQ = HangChuc + HangDV;
 * 
 * 
 * OutPut:
 * - Kết quả trả về màn hình là: 9
 * 
 *   */ 

var SoNhapVao = 18;
var HangChuc, HangDV;

var KQ = null;

HangDV = SoNhapVao % 10;
HangChuc = Math.floor(SoNhapVao/10);

console.log("BT5: Tính tổng 2 ký số.");
KQ = HangChuc + HangDV;
console.log('Tổng 2 số là: ', KQ);
