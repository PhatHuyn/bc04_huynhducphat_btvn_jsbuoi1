/**
 * BT3: Quy đổi tiền.
 * - Giá USD = 23.500 VNĐ.
 * 
 * Input:
 * - Người dùng nhập vào số tiền USD:
 * - TienUSD = 4 USD;
 * 
 * Todo:
 * S1: Tạo 1 biến lưu số tiền USD
 * S2: Tạo một biến lưu kết quả.
 * S3: Tính tiền USD sang tiền VNĐ
 * - Lấy: TienUSD * Giá USD (4 * 23.500).
 * 
 * 
 * OutPut:
 * - Kết quả trả về màn hình là: 94.000 VNĐ
 * 
 *   */ 

var TienUSD = 4;

var KQ = null;

console.log("BT3: Quy đổi tiền.");
KQ = TienUSD * 23500;
console.log('Tiền Việt Nam là: ', KQ );