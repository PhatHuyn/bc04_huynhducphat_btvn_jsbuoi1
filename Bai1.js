/**
 * BT1: Tính tiền lương nhân viên.
 * Lương 1 ngày 100.000
 * 
 * Input:
 * - Người dùng nhập vào số ngày làm.
 * - SoNgayLam = 30 ngày;
 * 
 * Todo:
 * S1: Tạo một biến lưu Số ngày làm
 * S2: Tạo một biến lưu Kết quả tính được
 * S3: Lấy số ngày làm * lương 1 ngày.
 * 
 * 
 * OutPut:
 * - Kết quả trả về màn hình là: 3000000
 * 
 *   */ 

var SoNgayLam = 30;

var KQ = null;

console.log("BT1: Tính tiền lương nhân viên.");
KQ = SoNgayLam * 100000;
console.log('Lương nhân viên là: ', KQ);