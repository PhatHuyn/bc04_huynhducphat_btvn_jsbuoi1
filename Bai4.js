/**
 * BT4: Tính diện tích, chu vi hình chữ nhật
 * 
 * 
 * Input:
 * - Người dùng nhập vào chiều dài và chiều rộng HCN:
 * - ChieuDai = 5 cm;
 * - ChieuRong = 9 cm;
 * 
 * Todo:
 * S1: Tạo 2 biến lưu chiều dài và chiều rộng hình chữ nhật.
 * S2: Tạo 2 biến lưu kết quả 
 * + Kết quả diện tích:
 * + Kết quả chu vi:
 * S3: Áp dụng công thức tính diện tích và chu vi hình chữ nhật
 * - Diện tích = ChieuDai * ChieuRong;
 * - Chu vi = (ChieuDai + ChieuRong) * 2;
 * 
 * 
 * OutPut:
 * - Kết quả trả về màn hình là:
 * + Diện dích: 45 cm;
 * + Chu vi: 28 cm;
 * 
 *   */ 

var ChieuDai = 5;
var ChieuRong = 9;

var KQDienTich = null;
var KQChuVi = null;

console.log("BT4: Tính diện tích, chu vi hình chữ nhật.");
KQDienTich = ChieuDai * ChieuRong;
console.log('Diện tích HCN là: ', KQDienTich);
KQChuVi = (ChieuDai + ChieuRong) * 2;
console.log('Chu vi HCN là: ', KQChuVi);



